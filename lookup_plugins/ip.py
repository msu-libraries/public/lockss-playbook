# Source: <https://stackoverflow.com/a/32324513/3716479> with modifications.

import ansible.errors as errors
from ansible.plugins.lookup import LookupBase
import socket

class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        if len(terms) != 1 or not isinstance(terms[0], str):
            raise errors.AnsibleError("bad invocation")

        return [socket.gethostbyname(terms[0])]
