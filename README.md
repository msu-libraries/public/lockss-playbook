# Ansible roles to deploy LOCKSS v2
This project provides Ansible roles, an example playbook, and a Vagrant
development configuration for deploying [LOCKSS](<https://www.lockss.org/>) v2
on a Ubuntu or CentOS host. This also adds an ingress controller that provides
access to LOCKSS's HTTP microservices through a single port, simplifying network
configuration.

## Role variables
### Required variables
* `lockss_hostname`: The hostname of the LOCKSS host.
* `lockss_ipaddr`: Probably the same as `lockss_external_ipaddr`.
* `lockss_external_ipaddr`: The external IP address of the LOCKSS host.
* `lockss_db_password`: The password for connecting to the internal PostgreSQL
  database. This should be machine-generated.
* `lockss_ui_password`
* `lockss_admin_email`

### Recommended but optional variables
* `lockss_network_ips`: A list of IP addresses and subnets that will be allowed
  to connect to LOCKSS's communications ports.
* `lockss_admin_ips`: A list of IP addresses and subnets that will be allowed to
  connect to LOCKSS's configuration pages.
* `lockss_props_url`: The URL to the LOCKSS network configuration (default in
  `roles/lockss/defaults/main.yml`).

### Optional variables
* `lockss_uid`: This and `lockss_gid` are intended for using a data directory
  mounted from another host.
* `lockss_gid`
* `lockss_git_version` (default in `roles/lockss/defaults/main.yml`)
* `lockss_mailhub_host` (default: `localhost`)
* `lockss_mailhub_user`
* `lockss_mailhub_password`
* `lockss_ui_user` (default in `roles/lockss/defaults/main.yml`)
* `lockss_data_dir` (default in `roles/lockss/defaults/main.yml`)
* `lockss_configure_firewall` (default: true)
* `lockss_frontend_port` (default in `roles/lockss/defaults/main.yml`)

## Network ports
LOCKSS runs as a collection of several microservices, each in its own Docker
container. These are the ports (all TCP) listened on by each service:

LOCKSS's services:

* metadata-extraction-service: 24640 (HTTP API), 24641 (HTTP config)
* metadata-service: 24650 (HTTP API), 24651 (HTTP config)
* repository-service: 24610 (HTTP API)
* configuration-service: 24620 (HTTP API), 24621 (HTTP config)
* poller: 9729 (LOCKSS), 24630 (HTTP API), 24631 (HTTP config), 24680 (HTTP
  config; labeled "Serve Content")
* postgres: 5432 (PostgreSQL interface)
* pywb: 8080 ([Pywb](<https://pypi.org/project/pywb/>) HTTP console)
* solr: 8983 (Solr HTTP console)

Service created by the `lockss` role:

* lockss-config-frontend: 80 (HTTP ingress controller)

Port 9729 needs to be reachable by the other nodes in the LOCKSS network. Only
local administrators should require access to any of the remaining ports. This
role creates an ingress controller
([`roles/http_frontend`](roles/http_frontend)) that maps all of the HTTP-based
services into a single directory space served at `http://<lockss_hostname>/`.
Thus, most administrators will need access only to port 80; however, the
firewall rules provisined by this role allow the IPs listed in
`lockss_admin_ips` to reach all of them.

## Example and production playbooks
Michigan State University Libraries' production playbook materials are included
under [`doc/playbook-example`](doc/playbook-example). For the convenience of
local users, the top-level files `production.playbook.yml`,
`production_secrets.yml`, `ansible.cfg`, and `hosts.yml`  are ignored by Git.
The included script `bin/generate-production-secrets` will securely generate a
new secrets file for production use.

## Developing with Vagrant
The included Vagrantfile will configure and run the playbook `dev.playbook.yml`
against the machines defined in `vagrant-machines.yml`, currently Ubuntu 18.04
and CentOS 7. If the `vagrant-hostsupdater` plugin is installed, Vagrant will
add appropriate entries to `/etc/hosts`, making the LOCKSS front-end page
accessible at `http://lockss-ubuntu.test` and `http://lockss-centos.test`.
Multiple base images are included to ensure that this role remains compatible
with each; for local testing, it's sufficient to use the one most closely
matching the intended production environment.
