---

# FIXME: None of this is idempotent.

- name: Create temporary directory.
  tempfile:
    state: directory
  changed_when: false
  register: tmpdir

- name: Copy httpd configuration file.
  template:
    dest: "{{ tmpdir.path }}/httpd-frontend.conf"
    src: httpd-frontend.conf.j2
    owner: root
    mode: 0644
  changed_when: false
  when: tmpdir.path is defined

- name: Determine name for httpd configuration file.
  block:
  - name: Register checksum of httpd configuration file.
    stat:
      path: "{{ tmpdir.path }}/httpd-frontend.conf"
      checksum_algorithm: sha256
    register: r

  - name: Calculate name.
    set_fact:
      _r2: "{{ http_frontend_name }}_\
           httpd_frontend_config-\
           {{ r.stat.checksum }}"

  - name: Calculate shorter name.
    set_fact:
      httpd_frontend_config_name: "{{ _r2 | regex_replace('(.{,64}).*',
                                   '\\1') }}"
  when: tmpdir.path is defined

- name: Copy landing page.
  template:
    dest: "{{ tmpdir.path }}/index.html"
    src: index.html.j2
    owner: root
    mode: 0644
  changed_when: false
  when: tmpdir.path is defined

- name: Determine name for landing page configuration file.
  block:
  - name: Register checksum of landing page.
    stat:
      path: "{{ tmpdir.path }}/index.html"
      checksum_algorithm: sha256
    register: r

  - name: Calculate name.
    set_fact:
      _r2: "{{ http_frontend_name }}_\
           index-\
           {{ r.stat.checksum }}"

  - name: Calculate shorter name.
    set_fact:
      index_name: "{{ _r2 | regex_replace('(.{,64}).*', '\\1') }}"
  when: tmpdir.path is defined

- name: Copy Dockerfile.
  copy:
    dest: "{{ tmpdir.path }}/Dockerfile"
    src: Dockerfile
    owner: root
    mode: 0644
  changed_when: false
  when: tmpdir.path is defined

- name: Install python3-jsondiff (Ubuntu).
  package:
    name: python3-jsondiff=1.*
    state: present
  when: ansible_distribution == "Ubuntu"

- name: Install needed Python libraries (CentOS).
  vars:
    ansible_python_interpreter: python3
  pip:
    name:
      - jsondiff>=1.0,<2.0
      - pyyaml>=5.0,<6.0
    state: present
  when: ansible_distribution == "CentOS"

- name: Build Docker image.
  docker_image:
    name: "{{ http_frontend_name }}:latest"
    state: present
    source: build
    force_source: true
    build:
      path: "{{ tmpdir.path }}"
      pull: yes
  when: tmpdir.path is defined

# Note: This won't trigger a redeployment when a new image is built because the
# new image will have the same tag ("latest") as the currently-running
# container.
- name: Deploy stack.
  vars:
    ansible_python_interpreter: python3
  docker_stack:
    name: "{{ http_frontend_name }}"
    state: present
    resolve_image: always
    prune: true
    compose:
      - version: "3.7"
        services:
          frontend:
            image: "{{ http_frontend_name }}:latest"
            configs:
              - source: httpd_frontend_config
                target: /usr/local/apache2/conf/conf.d/frontend.conf
              - source: index
                target: /usr/local/apache2/htdocs/index.html

        configs:
          httpd_frontend_config:
            name: "{{ httpd_frontend_config_name }}"
            file: "{{ tmpdir.path }}/httpd-frontend.conf"
          index:
            name: "{{ index_name }}"
            file: "{{ tmpdir.path }}/index.html"

        networks:
          default:
            external: true
            name: host
  when: tmpdir.path is defined

- name: Remove temporary directory.
  file:
    path: "{{ tmpdir.path }}"
    state: absent
  changed_when: false
  when: tmpdir.path is defined
