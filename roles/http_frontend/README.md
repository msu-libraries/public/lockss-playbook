# HTTP frontend

## Role variables
### Required variables
* `http_frontend_backends`: An array of dictionaries, each of which has a
  `memo` key (rendered as a title on the index page), a `name` key (used in the
  URL path), and a `port` key.
* `http_frontend_hostname`

### Optional variables
* `http_frontend_name`: The name used for the Docker image and stack. (Default in `defaults/main.yml`.)
* `http_frontend_port`: The port on which the service will listen. (Default in
  `defaults/main.yml`.)
* `http_frontend_index_title`: Give the index page a title.
