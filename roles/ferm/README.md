## Optional variables
* `ferm_skip_conflicts_in_check_mode`: Setting this to true will cause this role
  to skip the check for conflicting firewall managers when run in check mode.
  This is useful if another role would have removed them. (default: false)
